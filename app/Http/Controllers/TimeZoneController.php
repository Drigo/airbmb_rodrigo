<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TimeZone;

class TimeZoneController extends Controller
{
    public function index(){
        return TimeZone::all();
    }
}
