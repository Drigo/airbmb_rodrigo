<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    protected $table = 'state';
    protected $primaryKey ='id_state';
    public $fillable =[
            'name_state',
            'id_country'
    ];
}
