<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    protected $table = 'account';
    protected $primaryKey ='id_account';
    public $fillable =[
            'name_account',
            'active_account',
            'zip_account',
            'id_time_zone',
            'id_comunication',
            'id_state',
            'id_country',
            'id_type_account'
    ];
}

