<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/index', function () {
    return "Aqui va la pagina de inicio jajaja";
});

Route::get('/room', function () {
    return "aqui va para poner los cuartos en alquiler";
});

Route::get('/info_user', function () {
    return "aqui va la info del usuario";
});

Route::get('/rentas', function () {
    return "aqui va el alquier de cuartos";
});

Route::get('/info_rentas/{id_user}/{id_renta}', function ($id_user,$id_renta) {
    return "aca va los cuartos que alquilaste porel user".$id_user."rentoando la habitacion".$id_renta;
})->where('id_user','[0-9]+');

Route::get('/registro_user', function () {
    return "aca va el registro de un usuario";
});

Route::get('/registro_room', function () {
    return "aca va el registro de las habitaciones";
});

Route::get('/modificar_room/{id_room}', function ($id_room) {
    return "aca las modificaciones de las habitaciones";
});

Route::get('/modificar_user/{id_user}', function ($id_user) {
    return "aca las modificaciones del ususario". $id_user;
});

Route::get('/prueba', function () {
    return "pruebade middleware";
});